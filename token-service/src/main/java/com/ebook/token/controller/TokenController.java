package com.ebook.token.controller;

import com.ebook.dto.ResponseDto;
import com.ebook.pojo.User;
import com.ebook.token.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/token")
public class TokenController {
    @Autowired
    TokenService tokenService;
    @PostMapping("")
    public ResponseDto createToken(@RequestBody User user) {
        return tokenService.createToken(user);
    }

    @GetMapping("")
    public ResponseDto decodeToken(@RequestParam("token") String token) {
        return tokenService.decodeToken(token);
    }
}