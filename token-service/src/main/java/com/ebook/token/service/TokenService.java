package com.ebook.token.service;

import com.alibaba.fastjson2.JSONObject;
import com.ebook.dto.ResponseDto;
import com.ebook.pojo.User;

public interface TokenService {
    ResponseDto createToken(User user);
    ResponseDto decodeToken(String token);
}
