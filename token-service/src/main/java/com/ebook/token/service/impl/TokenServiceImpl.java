package com.ebook.token.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson2.JSONObject;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.ebook.dto.ResponseDto;
import com.ebook.pojo.User;
import com.ebook.token.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class TokenServiceImpl implements TokenService {
    private final static String SECRET="ebook@2024";

    @Override
    public ResponseDto createToken(User user){
        String token=JWT.create()
                .withClaim("id", user.getId())
                .withExpiresAt(DateUtil.offsetDay(DateUtil.date(),5))
                .sign(Algorithm.HMAC256(SECRET));
        JSONObject data=new JSONObject();
        data.put("token",token);
        return new ResponseDto(200,"成功",data);
    }

    @Override
    public ResponseDto decodeToken(String token){
        JSONObject data=new JSONObject();
        JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256(SECRET)).build();
        try{
            DecodedJWT decodedJWT = jwtVerifier.verify(token);
            data.put("id",decodedJWT.getClaim("id").asInt());
        }catch (TokenExpiredException e){
            return new ResponseDto(400,"Token已失效",null);
        }catch (Exception e){
            return new ResponseDto(400,"Token非法",null);
        }
        return new ResponseDto(200,"success",data);
    }
}
