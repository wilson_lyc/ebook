package com.ebook.captcha.controller;

import com.ebook.dto.ResponseDto;
import com.ebook.captcha.service.CaptchaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/captcha")
public class CaptchaController {
    @Autowired
    CaptchaService captchaService;
    @PostMapping("")
    public ResponseDto create(@RequestParam("email") String email) {
        return captchaService.create(email);
    }
    @GetMapping("")
    public ResponseDto verify(String captcha,String email){
        return captchaService.verify(email,captcha);
    }
}