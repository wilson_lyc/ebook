package com.ebook.captcha.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson2.JSONObject;
import com.ebook.captcha.feign.EmailFeignClient;
import com.ebook.captcha.service.CaptchaService;
import com.ebook.dto.ResponseDto;
import com.ebook.pojo.Email;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class CaptchaServiceImpl implements CaptchaService {
    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    EmailFeignClient emailFeignClient;

    @Override
    public ResponseDto create(String recipient) {
        //生成验证码
        String captcha = constructCaptcha();
        //保存验证码
        saveCaptcha(recipient, captcha);
        //发送邮件
        Email email = new Email();
        email.setRecipient(recipient);
        email.setContent(constructEmailContent(captcha));
        try {
            ResponseDto sendTextRes=emailFeignClient.sendText(email);
            if(sendTextRes.getCode()!=200)
                throw new Exception("邮件发送失败");
        }catch (Exception e){
            log.error("邮件发送失败",e);
            return new ResponseDto(500,"验证码发送失败",null);
        }
        return new ResponseDto(200,"验证码发送成功",null);
    }

    @Override
    public ResponseDto verify(String email,String captcha) {
        String realCaptcha = getCaptcha(email);
        if (realCaptcha == null) {
            return new ResponseDto(400, "验证码已过期", null);
        }
        if (!realCaptcha.equals(captcha)) {
            return new ResponseDto(400, "验证码错误", null);
        }
        return new ResponseDto(200, "验证成功", null);
    }

    private String constructCaptcha() {
        return String.valueOf(RandomUtil.randomInt(1000, 10000));
    }
    private String constructEmailContent(String captcha) {
        return "【公式E点通】用户您好！您的验证码是："+captcha+"。有效期5分钟。";
    }

    /**
     * 保存验证码到redis
     * @param email 邮箱
     * @param captcha 验证码
     */
    private void saveCaptcha(String email, String captcha) {
        String key = "captcha:" + email;
        //检查当前key是否存在
        if (redisTemplate.hasKey(key)) {
            redisTemplate.delete(key);
        }
        redisTemplate.opsForValue().set(key, captcha);
        //设置过期时间 5分钟
        redisTemplate.expire(key, 5, TimeUnit.MINUTES);
    }

    /**
     * 从redis获取验证码
     * @param email 邮箱
     * @return 验证码
     */
    private String getCaptcha(String email){
        String key="captcha:"+email;
        //检查当前key是否存在
        if(!redisTemplate.hasKey(key)){
            return null;
        }
        return (String) redisTemplate.opsForValue().get(key);
    }

}
