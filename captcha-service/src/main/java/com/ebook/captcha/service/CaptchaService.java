package com.ebook.captcha.service;

import com.ebook.dto.ResponseDto;
import com.ebook.pojo.User;

public interface CaptchaService{
    ResponseDto create(String email);
    ResponseDto verify(String email,String captcha);
}
