package com.ebook.captcha.feign;

import com.ebook.dto.ResponseDto;
import com.ebook.pojo.Email;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@FeignClient(name = "email-service")
public interface EmailFeignClient {
    @PostMapping("/email/text")
    ResponseDto sendText(@RequestBody Email email);
}
