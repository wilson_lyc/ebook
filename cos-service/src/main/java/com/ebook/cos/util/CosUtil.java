package com.ebook.cos.util;

import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson2.JSONObject;
import com.ebook.dto.ResponseDto;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.exception.CosClientException;
import com.qcloud.cos.exception.CosServiceException;
import com.qcloud.cos.http.HttpProtocol;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.UploadResult;
import com.qcloud.cos.region.Region;
import com.qcloud.cos.transfer.TransferManager;
import com.qcloud.cos.transfer.Upload;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@Component
public class CosUtil {
    @Value("${cos.bucket-name}")
    private String bucketName;
    @Value("${cos.secret.id}")
    private String secretId;
    @Value("${cos.secret.key}")
    private String secretKey;
    @Value("${cos.region}")
    private String region;
    @Value("${cos.thread-pool}")
    private int threadPoolNum;
    private COSClient createCosClient() {
        // 初始化用户身份信息
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        // 设置bucket的区域
        ClientConfig clientConfig = new ClientConfig(new Region(region));
        clientConfig.setHttpProtocol(HttpProtocol.https);
        // 生成cos客户端
        return new COSClient(cred, clientConfig);
    }

    public ResponseDto uploadFile(InputStream inputStream, String type) {
        // 文件名
        String key = IdUtil.simpleUUID()+"."+type;

        // 创建cos客户端
        COSClient cosclient = createCosClient();
        ExecutorService threadPool = Executors.newFixedThreadPool(threadPoolNum);
        TransferManager transferManager = new TransferManager(cosclient, threadPool);
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, inputStream, null);
        try {
            Upload upload = transferManager.upload(putObjectRequest);
            UploadResult uploadResult = upload.waitForUploadResult();
        } catch (CosServiceException e) {
            log.error("CosServiceException", e);
            return new ResponseDto(500, "上传失败");
        } catch (CosClientException e) {
            log.error("CosServiceException", e);
            return new ResponseDto(500, "上传失败");
        } catch (InterruptedException e) {
            log.error("InterruptedException", e);
            return new ResponseDto(500, "上传失败");
        }
        transferManager.shutdownNow();
        cosclient.shutdown();
        JSONObject data=new JSONObject();
        String BASE_URL = "https://"+bucketName+".cos.ap-guangzhou.myqcloud.com/";
        data.put("url", BASE_URL+key);
        return new ResponseDto(200, "上传成功",data);
    }
}
