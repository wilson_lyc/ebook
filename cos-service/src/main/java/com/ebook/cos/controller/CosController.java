package com.ebook.cos.controller;

import com.ebook.cos.service.CosService;
import com.ebook.dto.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/cos")
public class CosController {
    @Autowired
    CosService cosService;

    @PostMapping("/upload")
    public ResponseDto upload(@RequestParam("file") MultipartFile file, @RequestParam("type") String type) {
        return cosService.upload(file, type);
    }
}
