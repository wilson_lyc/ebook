package com.ebook.cos.interceptor;

import com.alibaba.fastjson2.JSONObject;
import com.ebook.dto.ResponseDto;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AuthInterceptor implements HandlerInterceptor {

    private RestTemplate restTemplate;

    public AuthInterceptor(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("Authorization");
        ResponseDto tokenRes;
        try{
            // 未携带token
            if(token == null || token.isEmpty()) {
                throw new Exception("未登录");
            }
            // 校验token合法性
            String url = "http://token-service/token?token=" + token;
            tokenRes= restTemplate.getForObject(url, ResponseDto.class);
            if (tokenRes.getCode() != 200) {
                throw new Exception("没有权限");
            }
        }catch (Exception e){
            response.setCharacterEncoding("UTF-8");
            response.setStatus(401);
            response.setContentType("application/json; charset=utf-8");
            ResponseDto responseDto = new ResponseDto(401, e.getMessage());
            response.getWriter().write(JSONObject.from(responseDto).toJSONString());
            return false;
        }
        // header里添加id
        response.setHeader("id", tokenRes.getData().getString("id"));
        return true;
    }
}
