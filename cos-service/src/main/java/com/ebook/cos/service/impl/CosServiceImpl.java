package com.ebook.cos.service.impl;

import com.ebook.cos.service.CosService;
import com.ebook.cos.util.CosUtil;
import com.ebook.dto.ResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Slf4j
@Service
public class CosServiceImpl implements CosService {
    @Autowired
    private CosUtil cosUtil;
    @Override
    public ResponseDto upload(MultipartFile file, String type) {
        ResponseDto uploadRes;
        try {
            uploadRes= cosUtil.uploadFile(file.getInputStream(), type);
        } catch (IOException e) {
            log.error("上传失败", e);
            return new ResponseDto(500, "上传失败");
        }
        return uploadRes;
    }
}
