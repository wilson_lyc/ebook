package com.ebook.cos.service;

import com.ebook.dto.ResponseDto;
import org.springframework.web.multipart.MultipartFile;

public interface CosService {
    ResponseDto upload(MultipartFile file, String type);
}
