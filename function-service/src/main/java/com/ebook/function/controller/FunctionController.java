package com.ebook.function.controller;

import com.ebook.dto.ResponseDto;
import com.ebook.function.service.FunctionService;
import com.ebook.pojo.Function;
import com.ebook.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/function")
public class FunctionController {
    @Autowired
    FunctionService functionService;
    @PostMapping("")
    public ResponseDto addFunction(Function function) {
        return functionService.addFunction(function);
    }
    @GetMapping("")
    public ResponseDto getFunction(String name,@RequestParam(defaultValue = "0") String cid, String category) {
        return functionService.getFunction(name, Integer.parseInt(cid), category);
    }
    @GetMapping("/{id}")
    public ResponseDto getFunctionById(@PathVariable("id") int id) {
        return functionService.getFunctionById(id);
    }
    @PutMapping("/{id}")
    public ResponseDto updateFunction(@PathVariable("id") int id, Function function) {
        function.setId(id);
        return functionService.updateFunction(function);
    }
    @GetMapping("/{id}/article")
    public ResponseDto getFunctionArticle(@PathVariable("id") int fid,@RequestHeader("Authorization") String token){
        return functionService.getFunctionArticle(fid,token);
    }
}
