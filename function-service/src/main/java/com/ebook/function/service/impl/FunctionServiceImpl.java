package com.ebook.function.service.impl;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ebook.dto.ResponseDto;
import com.ebook.function.feign.ArticleFeignClient;
import com.ebook.function.mapper.FuncArticleMapper;
import com.ebook.function.mapper.FunctionMapper;
import com.ebook.function.service.FunctionService;
import com.ebook.pojo.FuncArticle;
import com.ebook.pojo.Function;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Slf4j
@Service
public class FunctionServiceImpl implements FunctionService {

    @Autowired
    FunctionMapper functionMapper;
    @Autowired
    FuncArticleMapper funcArticleMapper;
    @Autowired
    ArticleFeignClient articleFeignClient;

    /**
     * 查询函数
     * @param name 函数名称
     * @param cid 分类id
     * @param category 分类
     * @return ResponseDto
     */
    @Override
    public ResponseDto getFunction(String name, int cid, String category) {
        List<Function> functions;
        try{
            QueryWrapper<Function> wrapper = new QueryWrapper<>();
            if (name != null && !name.isEmpty())
                wrapper.eq("name", name);
            if (cid != 0)
                wrapper.eq("cid", cid);
            if (category != null && !category.isEmpty())
                wrapper.eq("category", category);
            functions = functionMapper.selectList(wrapper);
        } catch (Exception e) {
            log.error("查询函数失败(mysql异常)", e);
            return new ResponseDto(500, "系统异常", null);
        }
        if (functions == null || functions.isEmpty()) {
            return new ResponseDto(404, "未找到相关函数", null);
        }
        JSONObject data = new JSONObject();
        data.put("functions",functions);
        return new ResponseDto(200, "成功", data);
    }

    /**
     * 根据函数id查询函数
     * @param id 函数id
     * @return ResponseDto
     */
    @Override
    public ResponseDto getFunctionById(int id) {
        Function function;
        try{
            function = functionMapper.selectById(id);
        } catch (Exception e) {
            log.error("查询函数失败(mysql异常)", e);
            return new ResponseDto(500, "系统异常", null);
        }
        if (function == null) {
            return new ResponseDto(404, "函数不存在", null);
        }
        JSONObject data = new JSONObject();
        data.put("function",function);
        return new ResponseDto(200, "成功", data);
    }

    @Override
    public ResponseDto addFunction(Function function) {
        try {
            functionMapper.insert(function);
        } catch (Exception e) {
            log.error("添加函数失败(mysql异常)", e);
            return new ResponseDto(500, "系统异常", null);
        }
        return new ResponseDto(200, "成功");
    }

    /**
     * 更新函数
     *
     * @param function 函数
     * @return ResponseDto
     */
    @Override
    public ResponseDto updateFunction(Function function) {
        try {
            functionMapper.updateById(function);
        } catch (Exception e) {
            log.error("更新函数失败(mysql异常)", e);
            return new ResponseDto(500, "系统异常", null);
        }
        return new ResponseDto(200, "成功");
    }

    @Override
    public ResponseDto getFunctionArticle(int fid, String token) {
        List<FuncArticle> funcArticles;
        try{
            QueryWrapper<FuncArticle> wrapper = new QueryWrapper<>();
            wrapper.eq("fid", fid);
            funcArticles = funcArticleMapper.selectList(wrapper);
        } catch (Exception e) {
            log.error("查询关联失败(mysql异常)", e);
            return new ResponseDto(500, "系统异常");
        }
        if (funcArticles == null || funcArticles.isEmpty()) {
            return new ResponseDto(404, "关联不存在");
        }
        JSONArray articleArray = new JSONArray();
        for (FuncArticle funcArticle : funcArticles) {
            ResponseDto articleRes = articleFeignClient.getArticleById(funcArticle.getAid(),token);
            JSONObject article = articleRes.getData().getJSONObject("article");
            JSONObject articleItem = new JSONObject();
            articleItem.put("id",article.getIntValue("id"));
            articleItem.put("title",article.getString("title"));
            articleArray.add(articleItem);
        }
        JSONObject data = new JSONObject();
        data.put("articles",articleArray);
        return new ResponseDto(200, "成功", data);
    }
}
