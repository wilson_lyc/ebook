package com.ebook.function.service;

import com.ebook.dto.ResponseDto;
import com.ebook.pojo.Function;

public interface FunctionService {
    ResponseDto getFunctionById(int id);
    ResponseDto getFunction(String name,int cid, String category);
    ResponseDto addFunction(Function function);

    ResponseDto updateFunction(Function function);

    ResponseDto getFunctionArticle(int fid, String token);
}
