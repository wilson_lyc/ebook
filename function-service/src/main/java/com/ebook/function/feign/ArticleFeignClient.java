package com.ebook.function.feign;

import com.ebook.dto.ResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

@Component
@FeignClient(name = "article-service")
public interface ArticleFeignClient {
    @GetMapping("/article/{id}")
    ResponseDto getArticleById(@PathVariable("id") int id, @RequestHeader("Authorization") String token);
}
