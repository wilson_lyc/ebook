package com.ebook.function.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ebook.pojo.Function;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface FunctionMapper extends BaseMapper<Function> {
}
