package com.ebook.function.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ebook.pojo.FuncArticle;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FuncArticleMapper extends BaseMapper<FuncArticle> {
}
