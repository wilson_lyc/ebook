/*
 Navicat Premium Data Transfer

 Source Server         : localhost3306
 Source Server Type    : MySQL
 Source Server Version : 80034
 Source Host           : localhost:3306
 Source Schema         : ebook

 Target Server Type    : MySQL
 Target Server Version : 80034
 File Encoding         : 65001

 Date: 30/06/2024 21:42:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标题',
  `uid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '作者',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '文章主要内容',
  `recommend` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否为精选文章',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of article
-- ----------------------------
BEGIN;
INSERT INTO `article` (`id`, `title`, `uid`, `content`, `recommend`, `create_time`, `update_time`) VALUES (1, '测试文章', '1', '我是测试文章', b'1', '2024-06-30 15:25:02', '2024-06-30 15:25:02');
INSERT INTO `article` (`id`, `title`, `uid`, `content`, `recommend`, `create_time`, `update_time`) VALUES (2, '测试文章2', '1', '我是测试文章', b'1', '2024-06-30 15:21:10', '2024-06-30 15:21:10');
COMMIT;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分类名称',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of category
-- ----------------------------
BEGIN;
INSERT INTO `category` (`id`, `name`, `create_time`, `update_time`) VALUES (1, '财务', '2024-06-09 23:22:24', '2024-06-09 23:22:24');
INSERT INTO `category` (`id`, `name`, `create_time`, `update_time`) VALUES (2, '日期', '2024-06-09 23:22:30', '2024-06-09 23:22:30');
INSERT INTO `category` (`id`, `name`, `create_time`, `update_time`) VALUES (3, '三角函数', '2024-06-09 23:22:36', '2024-06-09 23:22:36');
INSERT INTO `category` (`id`, `name`, `create_time`, `update_time`) VALUES (4, '统计', '2024-06-09 23:22:41', '2024-06-09 23:22:41');
INSERT INTO `category` (`id`, `name`, `create_time`, `update_time`) VALUES (5, '查找', '2024-06-09 23:22:53', '2024-06-09 23:22:53');
INSERT INTO `category` (`id`, `name`, `create_time`, `update_time`) VALUES (6, '数据库', '2024-06-09 23:22:58', '2024-06-09 23:22:58');
INSERT INTO `category` (`id`, `name`, `create_time`, `update_time`) VALUES (7, '文本', '2024-06-09 23:23:02', '2024-06-09 23:23:02');
INSERT INTO `category` (`id`, `name`, `create_time`, `update_time`) VALUES (8, '逻辑', '2024-06-09 23:23:07', '2024-06-09 23:23:07');
INSERT INTO `category` (`id`, `name`, `create_time`, `update_time`) VALUES (9, '信息', '2024-06-09 23:23:12', '2024-06-09 23:23:12');
COMMIT;

-- ----------------------------
-- Table structure for func
-- ----------------------------
DROP TABLE IF EXISTS `func`;
CREATE TABLE `func` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '函数id',
  `cid` int DEFAULT NULL COMMENT '函数类别id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '函数名称',
  `intro` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '功能简介',
  `tutorial` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT '使用教程',
  `video_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '教学视频',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of func
-- ----------------------------
BEGIN;
INSERT INTO `func` (`id`, `cid`, `name`, `intro`, `tutorial`, `video_url`, `create_time`, `update_time`) VALUES (1, 1, 'IRR', '算内部收益率', '# 参数:\n* Values：必须，为数组或单元格的引用，包含用来计算返回的内部收益率的数字。\n* Guess：必需，为对函数 IRR 计算结果的估计值。\n# 使用教程：\n1.  首先点击菜单栏插入函数，找到IRR函数，在弹出的对话框中总共有两项内容。分别是现金流和预估值。“现金流”是一组数据，包含用来计算返回内部报酬率的数字。“预估值”是内部报酬率的猜测值，不填默认为百分之十。现在要计算三年后的内部收益率。我们在“现金流”的位置选择三年的现金流数据，即A2至A5填入，“预估值”默认不填，点击确定，就算出投资三年后的内部收益率了。', 'https://www.wps.cn/learning/course/detail/id/149.html', '2024-06-28 21:03:02', '2024-06-28 21:03:04');
INSERT INTO `func` (`id`, `cid`, `name`, `intro`, `tutorial`, `video_url`, `create_time`, `update_time`) VALUES (2, 1, 'AMORDEGRC', '计算财务常用的折旧值', '# AMORDEGRC  函数计算财务常用的折旧值  # 参数  * Cost：必需， 资产原值。 * Date_purchased：必需，购入资产的日期。 * First_period：必需， 第一个期间结束时的日期。 * Salvage：必需，资产在使用寿命结束时的残值。 * Period：必需， 期间。 * Rate：必需， 折旧率。 * Basis：可选，要使用的年基准。  # 使用教程  1. 首先点击菜单栏插入函数，找到AMORDEGRC函数，此时弹出对话框，共六项参数：在运用这个函数的时候，我们需要输入6个值。在弹出的窗口中，选中原值，再选中（A2）填入。购入日期选中（A3），结束日期选中（A4），残值选中（A5），期间选中（A6），利率选中（A7）。基准选项一般不填，点击确定，非常快速的就求出折旧值了。2. 说明：默认情况下，1900年1月1日的序列号是1，而2008年1月1日的序列号是39448，这是因为它距1900年1月1日有39448天。  # 视频链接  *[AMORDEGRC函数视频教程](https://www.wps.cn/learning/course/detail/id/137.html)', 'https://www.wps.cn/learning/course/detail/id/137.html', '2024-06-30 10:08:42', '2024-06-30 10:08:42');
INSERT INTO `func` (`id`, `cid`, `name`, `intro`, `tutorial`, `video_url`, `create_time`, `update_time`) VALUES (3, 5, 'VOLOOKUP', '查询指定条件的结果', '# VOLOOKUP查询指定条件的结果# 参数* VLOOKUP(lookup_value,table_array,col_index_num,[range_lookup])* Lookup_value：为需要在数据表第一列中进行查找的数值。Lookup_value 可以为数值、引用或文本字符串。当vlookup函数第一参数省略查找值时，表示用0查找。* Table_array：为需要在其中查找数据的数据表。使用对区域或区域名称的引用。* col_index_num：为table_array 中查找数据的数据列序号。col_index_num 为 1 时，返回 table_array 第一列的数值，col_index_num 为 2 时，返回 table_array 第二列的数值，以此类推。如果 col_index_num 小于1，函数 VLOOKUP 返回错误值 #VALUE!；如果 col_index_num 大于 table_array 的列数，函数 VLOOKUP 返回错误值#REF!* Range_lookup：为一逻辑值，指明函数 VLOOKUP 查找时是精确匹配，还是近似匹配。如果为FALSE或0，则返回精确匹配，如果找不到，则返回错误值 #N/A。如果 range_lookup 为TRUE或1，函数 VLOOKUP 将查找近似匹配值，也就是说，如果找不到精确匹配值，则返回小于 lookup_value 的最大数值。如果range_lookup 省略，则默认为1。# 使用教程1. 首先点击菜单栏插入函数，找到VLOOKUP函数，点击确定，有四个参数，先填入或选中查找值，代表你所要查找的内容的条件，第二项为数据表，代表查找范围，但三项为列序数，代表你要返回查找范围内的第几列，第四项则为匹配条件，可选择大致查询还是精确查询。# 视频链接[VLOOKUP函数视频教程](https://www.wps.cn/learning/course/detail/id/46.html)<video width=\"320\" height=\"240\" controls>    <source src=\"https://res1.wpsacdm.cache.wpscdn.cn/videos/4845a14245c0d8bb223d7981d90b51b0.mp4\" type=\"video/mp4\"></video>', 'https://res1.wpsacdm.cache.wpscdn.cn/videos/4845a14245c0d8bb223d7981d90b51b0.mp4', '2024-06-30 10:41:46', '2024-06-30 10:48:17');
COMMIT;

-- ----------------------------
-- Table structure for func_article
-- ----------------------------
DROP TABLE IF EXISTS `func_article`;
CREATE TABLE `func_article` (
  `fid` int NOT NULL,
  `aid` int NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`fid`,`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of func_article
-- ----------------------------
BEGIN;
INSERT INTO `func_article` (`fid`, `aid`, `create_time`) VALUES (1, 1, '2024-06-30 15:40:12');
COMMIT;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '邮箱',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '密码',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '头像',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` (`id`, `email`, `password`, `nickname`, `avatar`, `create_time`, `update_time`) VALUES (1, 'wilson_lyc@qq.com', 'e10adc3949ba59abbe56e057f20f883e', 'wilson', NULL, '2024-06-28 15:25:41', '2024-06-30 13:30:51');
INSERT INTO `user` (`id`, `email`, `password`, `nickname`, `avatar`, `create_time`, `update_time`) VALUES (4, 'wilson_lyc@mail.com', '21218cca77804d2ba1922c33e0151105', 'EBook用户', 'https://ebook-1313412108.cos.ap-guangzhou.myqcloud.com/aebd2c431bf241b9be352631192b3557.jpg', '2024-06-30 14:46:26', '2024-06-30 21:15:11');
INSERT INTO `user` (`id`, `email`, `password`, `nickname`, `avatar`, `create_time`, `update_time`) VALUES (5, 'wilson_lyc@foxmail.com', '21218cca77804d2ba1922c33e0151105', 'lihua', 'https://ebook-1313412108.cos.ap-guangzhou.myqcloud.com/862e1c17b6624a62851c1e88e419ba90.jpg', '2024-06-30 21:15:38', '2024-06-30 21:19:04');
COMMIT;

-- ----------------------------
-- View structure for article_user
-- ----------------------------
DROP VIEW IF EXISTS `article_user`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `article_user` AS select `user`.`nickname` AS `author`,`article`.`id` AS `id`,`article`.`title` AS `title`,`article`.`uid` AS `uid`,`article`.`content` AS `content`,`article`.`recommend` AS `recommend`,`article`.`create_time` AS `create_time`,`article`.`update_time` AS `update_time` from (`article` join `user` on((`article`.`uid` = `user`.`id`)));

-- ----------------------------
-- View structure for func_cate
-- ----------------------------
DROP VIEW IF EXISTS `func_cate`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `func_cate` AS select `func`.`id` AS `id`,`func`.`cid` AS `cid`,`category`.`name` AS `category`,`func`.`name` AS `name`,`func`.`intro` AS `intro`,`func`.`tutorial` AS `tutorial`,`func`.`video_url` AS `video_url`,`func`.`create_time` AS `create_time`,`func`.`update_time` AS `update_time` from (`func` join `category` on((`func`.`cid` = `category`.`id`)));

SET FOREIGN_KEY_CHECKS = 1;
