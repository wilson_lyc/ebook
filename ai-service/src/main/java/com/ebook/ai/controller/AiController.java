package com.ebook.ai.controller;

import com.ebook.ai.service.Aiservice;
import com.ebook.dto.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ai")
public class AiController {
    @Autowired
    Aiservice aiservice;
    @PostMapping("")
    public ResponseDto ask(int id,String question) {
        return aiservice.ask(id,question);
    }
}
