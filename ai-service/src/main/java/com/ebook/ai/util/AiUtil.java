package com.ebook.ai.util;

import com.alibaba.dashscope.aigc.generation.Generation;
import com.alibaba.dashscope.aigc.generation.GenerationParam;
import com.alibaba.dashscope.aigc.generation.GenerationResult;
import com.alibaba.dashscope.exception.InputRequiredException;
import com.alibaba.dashscope.exception.NoApiKeyException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@RefreshScope
@Slf4j
public class AiUtil {
    @Value("${ai.api-key}")
    private String apiKey;
    @Async
    public void answerAsync(String question) {
        Generation gen = new Generation();
        GenerationParam param = GenerationParam.builder()
                .model("qwen-turbo")
                .apiKey(System.getenv(apiKey))
                .prompt(question)
                .build();
        GenerationResult result = null;
        try {
            result=gen.call(param);
        } catch (NoApiKeyException e) {
            log.error("No api key");
        } catch (InputRequiredException e) {
            log.error("Input required");
        }
        String output=result.getOutput().getText();
        System.out.println(output);
        System.out.println("Answered");
    }
    public String answer(String question) {
        Generation gen = new Generation();
        GenerationParam param = GenerationParam.builder()
                .model("qwen-turbo")
                .apiKey(System.getenv(apiKey))
                .prompt(question)
                .build();
        GenerationResult result = null;
        try {
            result=gen.call(param);
        } catch (NoApiKeyException e) {
            log.error("No api key");
            return null;
        } catch (InputRequiredException e) {
            log.error("Input required");
            return null;
        }
        return result.getOutput().getText();
    }
}
