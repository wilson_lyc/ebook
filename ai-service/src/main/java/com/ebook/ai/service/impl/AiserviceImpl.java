package com.ebook.ai.service.impl;

import com.alibaba.dashscope.aigc.generation.Generation;
import com.alibaba.dashscope.aigc.generation.GenerationParam;
import com.alibaba.dashscope.aigc.generation.GenerationResult;
import com.alibaba.dashscope.exception.InputRequiredException;
import com.alibaba.dashscope.exception.NoApiKeyException;
import com.alibaba.dashscope.utils.JsonUtils;
import com.alibaba.fastjson2.JSONObject;
import com.ebook.ai.service.Aiservice;
import com.ebook.ai.util.AiUtil;
import com.ebook.dto.ResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
@Slf4j
@Service
@RefreshScope
public class AiserviceImpl implements Aiservice {
    @Autowired
    private AiUtil aiUtil;

    /**
     * 提问ai
     * @param id 用户id
     * @param question 问题
     * @return 回答
     */
    @Override
    public ResponseDto ask(int id, String question) {
        String ans;
        try {
            ans=aiUtil.answer(question);
        } catch (Exception e) {
            log.error("提问失败", e);
            return new ResponseDto(500, "系统异常");
        }
        JSONObject data = new JSONObject();
        data.put("answer", ans);
        return new ResponseDto(200, "成功",data);
    }
}
