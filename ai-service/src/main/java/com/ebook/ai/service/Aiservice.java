package com.ebook.ai.service;

import com.ebook.dto.ResponseDto;

public interface Aiservice {
    ResponseDto ask(int id, String question);
}
