package com.ebook.category.controller;

import com.ebook.category.service.CategoryService;
import com.ebook.dto.ResponseDto;
import com.ebook.pojo.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;
    @PostMapping("")
    public ResponseDto addCategory(Category category) {
        return categoryService.addCategory(category);
    }
    @GetMapping("")
    public ResponseDto getCategory() {
        return categoryService.getCategory();
    }
    @GetMapping("/catalog")
    public ResponseDto getCatalog(@RequestHeader("Authorization") String token) {
        return categoryService.getCatalog(token);
    }
}
