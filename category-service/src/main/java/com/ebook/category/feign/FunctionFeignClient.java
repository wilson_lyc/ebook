package com.ebook.category.feign;

import com.ebook.dto.ResponseDto;
import com.ebook.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(name = "function-service")
public interface FunctionFeignClient {
    @GetMapping("/function")
    ResponseDto getFunction(
            @RequestParam("name")String name,
            @RequestParam("cid") String cid,
            @RequestParam("category") String category,
            @RequestHeader("Authorization") String token);
}
