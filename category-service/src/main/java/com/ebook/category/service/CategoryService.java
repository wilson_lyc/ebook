package com.ebook.category.service;

import com.ebook.dto.ResponseDto;
import com.ebook.pojo.Category;

public interface CategoryService {
    ResponseDto getCategory();

    ResponseDto getCatalog(String token);

    ResponseDto addCategory(Category category);
}
