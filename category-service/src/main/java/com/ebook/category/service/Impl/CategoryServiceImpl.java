package com.ebook.category.service.Impl;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.ebook.category.feign.FunctionFeignClient;
import com.ebook.category.mapper.CategoryMapper;
import com.ebook.category.service.CategoryService;
import com.ebook.dto.ResponseDto;
import com.ebook.pojo.Category;
import com.ebook.pojo.Function;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Slf4j
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryMapper categoryMapper;
    @Autowired
    FunctionFeignClient functionFeignClient;

    /**
     * 查询所有函数类别
     * @return ResponseDto
     */
    @Override
    public ResponseDto getCategory() {
        List<Category> categories;
        try{
            categories = categoryMapper.selectList(null);
        }catch (Exception e){
            log.error("查询函数分类失败(mysql异常)", e);
            return new ResponseDto(500, "系统异常", null);
        }
        JSONObject data = new JSONObject();
        data.put("categories", categories);
        return new ResponseDto(200, "成功", data);
    }

    /**
     * 查询所有函数目录
     * @return ResponseDto
     */
    @Override
    public ResponseDto getCatalog(String token) {
        List<Category> categories;
        JSONArray catelog = new JSONArray();
        try{
            categories = categoryMapper.selectList(null);
        }catch (Exception e){
            log.error("查询函数分类失败(mysql异常)", e);
            return new ResponseDto(500, "系统异常", null);
        }
        try{
            for (Category category : categories) {
                ResponseDto funcRes= functionFeignClient.getFunction(null, String.valueOf(category.getId()), null, token);
                JSONObject catalogItem = JSONObject.from(category);
                JSONObject data = funcRes.getData();
                JSONArray funcArray;
                if (data==null) {
                    funcArray= new JSONArray();
                }else {
                    funcArray = data.getJSONArray("functions");
                }
                catalogItem.put("children", funcArray);
                catelog.add(catalogItem);
            }
        }catch (Exception e){
            log.error("查询函数目录失败", e);
            return new ResponseDto(500, "系统异常", null);
        }
        JSONObject data = new JSONObject();
        data.put("catalog", catelog);
        return new ResponseDto(200, "成功", data);
    }

    /**
     * 添加函数类别
     * @param category 函数类别
     * @return ResponseDto
     */
    @Override
    public ResponseDto addCategory(Category category) {
        try{
            categoryMapper.insert(category);
        }catch (Exception e){
            log.error("添加函数分类失败(mysql异常)", e);
            return new ResponseDto(500, "系统异常", null);
        }
        return new ResponseDto(200, "成功", null);
    }
}
