package com.ebook.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@TableName("article_user")
public class Article {
    private int id;
    private String title;
    private int uid;
    private String author;
    private String content;
    private int recommend;
    private String createTime;
    private String updateTime;
}