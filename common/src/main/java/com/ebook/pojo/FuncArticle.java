package com.ebook.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FuncArticle {
    private int aid;
    private int fid;
    private String createTime;
}
