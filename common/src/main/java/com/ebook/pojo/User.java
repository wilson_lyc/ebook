package com.ebook.pojo;

import com.alibaba.fastjson2.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User {
    private int id;
    private String email;
    private String password;
    private String nickname;
    private String avatar;
    private String createTime;
    private String updateTime;

    public JSONObject toJson() {
        JSONObject json = JSONObject.from(this);
        json.remove("password");
        return json;
    }
}
