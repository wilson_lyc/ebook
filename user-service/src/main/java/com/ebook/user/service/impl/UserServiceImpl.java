package com.ebook.user.service.impl;

import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ebook.dto.ResponseDto;
import com.ebook.pojo.User;
import com.ebook.user.dto.*;
import com.ebook.user.feign.CaptchaFeignClient;
import com.ebook.user.feign.CosFeignClient;
import com.ebook.user.feign.TokenFeignClient;
import com.ebook.user.mapper.UserMapper;
import com.ebook.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Slf4j
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;
    @Autowired
    TokenFeignClient tokenFeignClient;
    @Autowired
    CaptchaFeignClient captchaFeignClient;
    @Autowired
    CosFeignClient cosFeignClient;

    private boolean verifyCaptcha(String email, String captcha) throws Exception {
        try{
            ResponseDto captchaRes = captchaFeignClient.verify(captcha,email);
            if(captchaRes.getCode() != 200) {
                return false;
            }
        }catch (Exception e){
            log.error("验证码服务异常",e);
            throw new Exception("验证码服务异常");
        }
        return true;
    }

    /**
     * 注册
     * @param email 邮箱
     * @param password 密码
     * @param captcha 验证码
     * @return ResponseDto
     */
    @Override
    public ResponseDto register(String email, String password, String captcha) {
        //验证邮箱是否已注册
        if(userMapper.selectByEmail(email) != null) {
            return new ResponseDto(400,"邮箱已注册",null);
        }
        //验证验证码
        try {
            if (!verifyCaptcha(email,captcha)) {
                return new ResponseDto(400,"验证码错误",null);
            }
        } catch (Exception e) {
            return new ResponseDto(500,"系统错误",null);
        }
        //新增用户
        try{
            User user = new User();
            user.setEmail(email);
            user.setPassword(SecureUtil.md5(password));
            userMapper.insert(user);
        }catch (Exception e){
            log.error("注册失败(mysql异常)",e);
            return new ResponseDto(500,"系统异常",null);
        }
        return new ResponseDto(200,"成功",null);
    }


    /**
     * 密码登录
     * @param email 邮箱
     * @param password 密码
     * @return ResponseDto
     */
    @Override
    public ResponseDto loginByPass(String email, String password) {
        User user = userMapper.selectByEmail(email);
        if(user == null) {
            return new ResponseDto(404,"用户不存在",null);
        }
        password = SecureUtil.md5(password);
        if(!user.getPassword().equals(password)) {
            return new ResponseDto(400,"密码错误",null);
        }
        ResponseDto tokenJson = tokenFeignClient.createToken(user);
        JSONObject data = new JSONObject();
        data.put("user",user.toJson());
        data.put("token",tokenJson.getData().getString("token"));
        return new ResponseDto(200,"登录成功",data);
    }

    /**
     * 验证码登录
     * @param email 邮箱
     * @param captcha 验证码
     * @return ResponseDto
     */
    @Override
    public ResponseDto loginByCaptcha(String email, String captcha) {
        User user = userMapper.selectByEmail(email);
        if(user == null) {
            return new ResponseDto(404,"用户不存在",null);
        }
        //验证验证码
        try {
            if (!verifyCaptcha(email,captcha)) {
                return new ResponseDto(400,"验证码错误",null);
            }
        } catch (Exception e) {
            return new ResponseDto(500,"系统错误",null);
        }
        //登录成功
        ResponseDto tokenJson;
        try{
            tokenJson = tokenFeignClient.createToken(user);
        }catch (Exception e){
            log.error("登录失败（token服务异常）",e);
            return new ResponseDto(500,"登录失败",null);
        }
        JSONObject data = new JSONObject();
        data.put("user",user.toJson());
        data.put("token",tokenJson.getData().getString("token"));
        return new ResponseDto(200,"登录成功",data);
    }

    /**
     * 更新用户信息
     * @param user 用户
     * @return ResponseDto
     */
    @Override
    public ResponseDto updateInfo(User user) {
        user.setEmail(null);
        user.setPassword(null);
        try{
            if(userMapper.updateById(user)==0){
                return new ResponseDto(404,"用户不存在",null);
            }
        }catch (Exception e){
            log.error("更新用户信息失败(mysql错误)",e);
            return new ResponseDto(500,"更新用户信息失败",null);
        }
        return new ResponseDto(200,"成功",null);
    }

    /**
     * 更新密码
     * @param id 用户id
     * @param oldPass 旧密码
     * @param newPass 新密码
     * @return ResponseDto
     */
    @Override
    public ResponseDto updatePass(int id, String oldPass, String newPass) {
        User user = userMapper.selectById(id);
        if(user == null) {
            return new ResponseDto(404,"用户不存在",null);
        }
        //验证旧密码
        oldPass = SecureUtil.md5(oldPass);
        if(!user.getPassword().equals(oldPass)) {
            return new ResponseDto(400,"旧密码错误",null);
        }
        //更新密码
        try{
            user.setPassword(SecureUtil.md5(newPass));
            userMapper.updateById(user);
        }catch (Exception e){
            log.error("更新密码失败(mysql错误)",e);
            return new ResponseDto(500,"更新密码失败",null);
        }
        return new ResponseDto(200,"成功",null);
    }

    /**
     * 更新邮箱
     * @param id 用户id
     * @param email 新邮箱
     * @param captcha 验证码
     * @return ResponseDto
     */
    @Override
    public ResponseDto updateEmail(int id, String email, String captcha) {
        User user = userMapper.selectById(id);
        if(user == null) {
            return new ResponseDto(404,"用户不存在",null);
        }
        //验证邮箱是否已注册
        if(userMapper.selectByEmail(email) != null) {
            return new ResponseDto(400,"新邮箱已被其他用户注册",null);
        }
        //验证验证码
        try {
            if (!verifyCaptcha(email,captcha)) {
                return new ResponseDto(400,"验证码错误",null);
            }
        } catch (Exception e) {
            return new ResponseDto(500,"系统错误",null);
        }
        //更新邮箱
        try{
            user.setEmail(email);
            userMapper.updateById(user);
        }catch (Exception e){
            log.error("更新邮箱失败(mysql错误)",e);
            return new ResponseDto(500,"更新邮箱失败",null);
        }
        return new ResponseDto(200,"成功",null);
    }

    /**
     * 上传头像
     * @param id 用户id
     * @param file 头像文件
     * @param type 类型
     * @return ResponseDto
     */
    @Override
    public ResponseDto uploadAvatar(int id, MultipartFile file, String type,String token) {
        ResponseDto uploadRes;
        //上传头像
        try{
            uploadRes=cosFeignClient.upload(file,type,token);
            if (uploadRes.getCode() != 200) {
                throw new Exception("上传头像失败(cos服务异常)");
            }
        }catch (Exception e){
            log.error("上传头像失败(cos服务异常) ",e);
            return new ResponseDto(500,"上传头像失败",null);
        }
        //更新用户头像
        try{
            User user = new User();
            user.setId(id);
            user.setAvatar(uploadRes.getData().getString("url"));
            userMapper.updateById(user);
        }catch (Exception e){
            log.error("更新头像失败(mysql异常)",e);
            return new ResponseDto(500,"系统异常",null);
        }
        return new ResponseDto(200,"成功",null);
    }

    /**
     * 根据id获取用户信息
     * @param id 用户id
     * @return ResponseDto
     */
    @Override
    public ResponseDto getUserInfoById(int id) {
        User user = userMapper.selectById(id);
        if(user == null) {
            return new ResponseDto(404,"用户不存在",null);
        }
        JSONObject data = new JSONObject();
        data.put("user",user.toJson());
        return new ResponseDto(200,"成功",data);
    }

    /**
     * 查找用户信息
     * @return ResponseDto
     */
    @Override
    public ResponseDto getUser(String nickname) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        if (nickname != null && !nickname.isEmpty()) {
            wrapper.like("nickname",nickname);
        }
        List<User> userList = userMapper.selectList(wrapper);
        JSONObject data = new JSONObject();
        JSONArray users = new JSONArray();
        for(User user : userList) {
            users.add(user.toJson());
        }
        data.put("users",users);
        return new ResponseDto(200,"成功",data);
    }
}
