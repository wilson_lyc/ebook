package com.ebook.user.service;

import com.ebook.dto.ResponseDto;
import com.ebook.pojo.User;
import com.ebook.user.dto.*;
import org.springframework.web.multipart.MultipartFile;

public interface UserService {
    ResponseDto register(String email, String password, String captcha);
    ResponseDto loginByPass(String email, String password);
    ResponseDto loginByCaptcha(String email, String captcha);
    ResponseDto updateEmail(int id, String email, String captcha);
    ResponseDto uploadAvatar(int id, MultipartFile file, String type,String token);

    ResponseDto getUserInfoById(int id);

    ResponseDto getUser(String nickname);

    ResponseDto updateInfo(User user);

    ResponseDto updatePass(int id, String oldPass, String newPass);
}
