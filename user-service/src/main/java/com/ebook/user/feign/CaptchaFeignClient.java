package com.ebook.user.feign;

import com.ebook.dto.ResponseDto;
import com.ebook.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(name = "captcha-service")
public interface CaptchaFeignClient {
    @GetMapping("/captcha")
    ResponseDto verify(
            @RequestParam("captcha") String captcha,
            @RequestParam("email") String email);
}
