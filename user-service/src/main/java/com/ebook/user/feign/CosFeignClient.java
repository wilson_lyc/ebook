package com.ebook.user.feign;

import com.ebook.dto.ResponseDto;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Component
@FeignClient(name = "cos-service")
public interface CosFeignClient {
    @RequestMapping(
            value = "/cos/upload",
            method = RequestMethod.POST,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ResponseDto upload(@RequestPart("file")MultipartFile file,@RequestParam("type")String type,@RequestHeader("Authorization") String token);
}
