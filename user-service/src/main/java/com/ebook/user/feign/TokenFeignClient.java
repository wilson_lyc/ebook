package com.ebook.user.feign;

import com.alibaba.fastjson2.JSONObject;
import com.ebook.dto.ResponseDto;
import com.ebook.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Component
@FeignClient(name = "token-service")
public interface TokenFeignClient {
    @PostMapping("/token")
    ResponseDto createToken(@RequestBody User user);
    @GetMapping("/token")
    ResponseDto decodeToken(@RequestParam("token") String token);
}
