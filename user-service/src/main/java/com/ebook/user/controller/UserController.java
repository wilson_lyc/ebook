package com.ebook.user.controller;

import com.ebook.dto.*;
import com.ebook.pojo.User;
import com.ebook.user.dto.*;
import com.ebook.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;
    @PostMapping("/register")
    public ResponseDto register(@RequestParam("email") String email,@RequestParam("password") String password,@RequestParam("captcha") String captcha) {
        return userService.register(email,password,captcha);
    }
    @PostMapping("/login/pass")
    public ResponseDto loginByPass(@RequestParam("email") String email,@RequestParam("password") String password) {
        return userService.loginByPass(email,password);
    }
    @PostMapping("/login/captcha")
    public ResponseDto loginByCaptcha(@RequestParam("email") String email,@RequestParam("captcha") String captcha) {
        return userService.loginByCaptcha(email,captcha);
    }
    @PutMapping("/{id}")
    public ResponseDto updateInfo(@PathVariable("id")int id,User user) {
        user.setId(id);
        return userService.updateInfo(user);
    }
    @PutMapping("/{id}/pass")
    public ResponseDto updatePass(@PathVariable("id")int id,@RequestParam("oldPass") String oldPass,@RequestParam("newPass") String newPass) {
        return userService.updatePass(id,oldPass,newPass);
    }
    @PutMapping("/{id}/email")
    public ResponseDto updateEmail(@PathVariable("id")int id,@RequestParam("email") String email,@RequestParam("captcha") String captcha) {
        return userService.updateEmail(id,email,captcha);
    }
    @PutMapping("/{id}/avatar")
    public ResponseDto uploadAvatar(
            @PathVariable("id")int id,
            @RequestParam("avatar") MultipartFile file,
            @RequestParam("type") String type,
            @RequestHeader("Authorization") String token) {
        return userService.uploadAvatar(id,file,type,token);
    }
    @GetMapping("/{id}")
    public ResponseDto getUserInfoById(@PathVariable("id") int id) {
        return userService.getUserInfoById(id);
    }
    @GetMapping("")
    public ResponseDto getAllUser(String nickname) {
        return userService.getUser(nickname);
    }
}