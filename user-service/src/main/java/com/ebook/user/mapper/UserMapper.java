package com.ebook.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ebook.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper extends BaseMapper<User> {
    @Select("select * from user where email=#{email}")
    User selectByEmail(String email);
}