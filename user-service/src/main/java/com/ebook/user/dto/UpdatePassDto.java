package com.ebook.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdatePassDto {
    @NotBlank
    private String oldPass;
    @NotBlank
    private String newPass;
    @NotBlank
    private String captcha;
}
