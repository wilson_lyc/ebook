package com.ebook.email.service.impl;

import cn.hutool.extra.mail.Mail;
import cn.hutool.extra.mail.MailAccount;
import com.alibaba.fastjson2.JSONObject;
import com.ebook.dto.ResponseDto;
import com.ebook.email.service.EmailService;
import com.ebook.pojo.Email;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import cn.hutool.extra.mail.MailException;

@Slf4j
@Service
@RefreshScope
public class EmailServiceImpl implements EmailService {
    @Value("${mail.host}")
    private String host;
    @Value("${mail.port}")
    private int port;
    @Value("${mail.from}")
    private String from;
    @Value("${mail.user}")
    private String user;
    @Value("${mail.pass}")
    private String password;
    @Override
    public ResponseDto sendText(Email email) {
        MailAccount mailAccount = new MailAccount();
        mailAccount.setHost(host);
        mailAccount.setPort(port);
        mailAccount.setAuth(true);
        mailAccount.setFrom(from);
        mailAccount.setUser(user);
        mailAccount.setPass(password);
        try{
            Mail mail = Mail.create(mailAccount)
                    .setTos(email.getRecipient())
                    .setTitle(email.getSubject())
                    .setContent(email.getContent())
                    .setHtml(email.isHtml());
            mail.send();
        }catch (MailException e){
            log.error("邮件发送失败",e);
            return new ResponseDto(500,"邮件发送失败");
        }
        return new ResponseDto(200,"邮件发送成功");
    }
}
