package com.ebook.email.contoller;

import com.ebook.dto.ResponseDto;
import com.ebook.email.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.ebook.pojo.Email;

@RestController
@RequestMapping("/email")
public class EmailController {
    @Autowired
    EmailService emailService;

    @PostMapping("/text")
    public ResponseDto sendText(@RequestBody Email email) {
        return emailService.sendText(email);
    }
}

