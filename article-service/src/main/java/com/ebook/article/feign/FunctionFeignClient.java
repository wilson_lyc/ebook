package com.ebook.article.feign;

import com.ebook.dto.ResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(name = "function-service")
public interface FunctionFeignClient {
    @GetMapping("/function/{id}")
    ResponseDto getFunctionById(@PathVariable("id") int id,@RequestHeader("Authorization") String token);
}
