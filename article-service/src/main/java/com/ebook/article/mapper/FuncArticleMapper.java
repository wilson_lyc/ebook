package com.ebook.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ebook.pojo.Article;
import com.ebook.pojo.FuncArticle;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FuncArticleMapper extends BaseMapper<FuncArticle> {
}
