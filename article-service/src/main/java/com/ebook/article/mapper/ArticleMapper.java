package com.ebook.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ebook.pojo.Article;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface ArticleMapper extends BaseMapper<Article> {
}
