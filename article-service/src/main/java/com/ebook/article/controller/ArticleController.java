package com.ebook.article.controller;

import com.ebook.article.service.ArticleService;
import com.ebook.dto.ResponseDto;
import com.ebook.pojo.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/article")
public class ArticleController {
    @Autowired
    private ArticleService articleService;
    @PostMapping("")
    ResponseDto addArticle(Article article) {
        return articleService.addArticle(article);
    }
    @GetMapping("")
    ResponseDto getArticle(@RequestParam(defaultValue = "0") String uid, String title,String content) {
        return articleService.getArticle(Integer.parseInt(uid), title,content);
    }
    @GetMapping("/{id}")
    ResponseDto getArticleById(@PathVariable("id") int id) {
        return articleService.getArticleById(id);
    }
    @GetMapping("/recommend")
    ResponseDto getRecommendArticle() {
        return articleService.getRecommendArticle();
    }
    @PutMapping("/{id}")
    ResponseDto updateArticle(@PathVariable("id") int id, Article article) {
        article.setId(id);
        return articleService.updateArticle(article);
    }
    @PutMapping("/{id}/recommend")
    ResponseDto updateRecommend(@PathVariable("id") int id, @RequestParam("recommend") int recommend) {
        return articleService.updateRecommend(id, recommend);
    }
    @PostMapping("/relation/{aid}/{fid}")
    ResponseDto addRelation(@PathVariable("aid") int aid, @PathVariable("fid") int fid) {
        return articleService.addRelation(aid, fid);
    }

    @DeleteMapping("/relation/{aid}/{fid}")
    ResponseDto deleteRelation(@PathVariable("aid") int aid, @PathVariable("fid") int fid) {
        return articleService.deleteRelation(aid, fid);
    }

    @GetMapping("/{id}/function")
    public ResponseDto getArticleRelation(@PathVariable("id") int aid, @RequestHeader("Authorization") String token){
        return articleService.getArticleRelation(aid,token);
    }
}
