package com.ebook.article.service;


import com.ebook.dto.ResponseDto;
import com.ebook.pojo.Article;

public interface ArticleService {

    ResponseDto getRecommendArticle();

    ResponseDto getArticle(int uid, String title, String content);

    ResponseDto getArticleById(int id);

    ResponseDto addArticle(Article article);

    ResponseDto updateArticle(Article article);

    ResponseDto updateRecommend(int id, int recommend);

    ResponseDto addRelation(int aid, int fid);

    ResponseDto deleteRelation(int aid, int fid);


    ResponseDto getArticleRelation(int aid, String token);
}
