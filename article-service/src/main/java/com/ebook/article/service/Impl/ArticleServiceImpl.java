package com.ebook.article.service.Impl;


import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ebook.article.feign.FunctionFeignClient;
import com.ebook.article.mapper.ArticleMapper;
import com.ebook.article.mapper.FuncArticleMapper;
import com.ebook.article.service.ArticleService;
import com.ebook.dto.ResponseDto;
import com.ebook.pojo.Article;
import com.ebook.pojo.FuncArticle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleMapper articleMapper;
    @Autowired
    private FuncArticleMapper funcArticleMapper;
    @Autowired
    private FunctionFeignClient functionFeignClient;

    /**
     * 获取推荐文章
     * @return ResponseDto
     */
    @Override
    public ResponseDto getRecommendArticle() {

        List<Article> articles;
        try{
            QueryWrapper<Article> wrapper = new QueryWrapper<>();
            wrapper.eq("recommend", 1);
            articles = articleMapper.selectList(wrapper);
        } catch (Exception e) {
            log.error("查询精选文章失败-mysql", e);
            return new ResponseDto(500, "查询失败");
        }
        JSONObject data = new JSONObject();
        data.put("article",articles);
        return new ResponseDto(200, "成功", data);
    }

    /**
     * 查询文章
     * @return ResponseDto
     */
    @Override
    public ResponseDto getArticle(int uid, String title, String content) {
        List<Article> articles;
        try{
            QueryWrapper<Article> wrapper = new QueryWrapper<>();
            if (uid>0)
                wrapper.eq("uid", uid);
            if (title!=null && !title.isEmpty())
                wrapper.like("title", title);
            if (content!=null && !content.isEmpty())
                wrapper.like("content", content);
            articles = articleMapper.selectList(wrapper);
        } catch (Exception e) {
            log.error("查询文章失败(mysql异常)", e);
            return new ResponseDto(500, "系统异常");
        }
        if (articles == null|| articles.isEmpty()) {
            return new ResponseDto(400, "文章不存在");
        }
        JSONObject data = new JSONObject();
        data.put("articles",articles);
        return new ResponseDto(200, "成功", data);
    }

    /**
     * 根据id查询文章
     * @param id 文章id
     * @return ResponseDto
     */
    @Override
    public ResponseDto getArticleById(int id) {
        Article article;
        try{
            article = articleMapper.selectById(id);
        } catch (Exception e) {
            log.error("查询文章失败(mysql异常)", e);
            return new ResponseDto(500, "系统异常");
        }
        if (article == null) {
            return new ResponseDto(404, "文章不存在");
        }
        JSONObject data = new JSONObject();
        data.put("article",article);
        return new ResponseDto(200, "成功", data);
    }

    /**
     * 添加文章
     * @param article 文章
     * @return ResponseDto
     */
    @Override
    public ResponseDto addArticle(Article article) {
        try{
            articleMapper.insert(article);
        } catch (Exception e) {
            log.error("添加文章失败(mysql异常)", e);
            return new ResponseDto(500, "系统异常");
        }
        return new ResponseDto(200, "成功");
    }

    /**
     * 更新文章
     * @param article 文章
     * @return ResponseDto
     */
    @Override
    public ResponseDto updateArticle(Article article) {
        try{
            UpdateWrapper<Article> wrapper = new UpdateWrapper<>();
            wrapper.eq("id", article.getId());
            wrapper.set("title", article.getTitle());
            wrapper.set("content", article.getContent());
            articleMapper.update(null, wrapper);
        } catch (Exception e) {
            log.error("更新文章失败(mysql异常)", e);
            return new ResponseDto(500, "系统异常");
        }
        return new ResponseDto(200, "成功");
    }

    /**
     * 更新推荐状态
     * @param id 文章id
     * @param recommend 推荐状态
     * @return ResponseDto
     */
    @Override
    public ResponseDto updateRecommend(int id, int recommend) {
        UpdateWrapper<Article> wrapper = new UpdateWrapper<>();
        wrapper.eq("id", id);
        wrapper.set("recommend", recommend);
        try{
            if (articleMapper.update(null, wrapper) == 0) {
                return new ResponseDto(404, "文章不存在");
            }
        } catch (Exception e) {
            log.error("更新推荐状态失败(mysql异常)", e);
            return new ResponseDto(500, "系统异常");
        }
        return new ResponseDto(200, "成功");
    }

    /**
     * 添加关联
     * @param aid 文章id
     * @param fid 函数id
     * @return ResponseDto
     */
    @Override
    public ResponseDto addRelation(int aid, int fid) {
        try{
            funcArticleMapper.insert(new FuncArticle(aid,fid,null));
        }catch (Exception e){
            log.error("添加关联失败(mysql异常)", e);
            return new ResponseDto(500, "系统异常");
        }
        return new ResponseDto(200, "成功");
    }

    /**
     * 删除关联
     * @param aid 文章id
     * @param fid 函数
     * @return ResponseDto
     */
    @Override
    public ResponseDto deleteRelation(int aid, int fid) {
        try{
            QueryWrapper<FuncArticle> wrapper = new QueryWrapper<>();
            wrapper.eq("aid", aid);
            wrapper.eq("fid", fid);
            funcArticleMapper.delete(wrapper);
        }catch (Exception e){
            log.error("删除关联失败(mysql异常)", e);
            return new ResponseDto(500, "系统异常");
        }
        return new ResponseDto(200, "成功");
    }

    @Override
    public ResponseDto getArticleRelation(int aid, String token) {
        List<FuncArticle> funcArticles;
        try{
            QueryWrapper<FuncArticle> wrapper = new QueryWrapper<>();
            wrapper.eq("aid", aid);
            funcArticles = funcArticleMapper.selectList(wrapper);
        } catch (Exception e) {
            log.error("查询关联失败(mysql异常)", e);
            return new ResponseDto(500, "系统异常");
        }
        if (funcArticles == null || funcArticles.isEmpty()) {
            return new ResponseDto(404, "关联不存在");
        }
        JSONArray funcArray = new JSONArray();
        for (FuncArticle funcArticle : funcArticles) {
            ResponseDto funcRes = functionFeignClient.getFunctionById(funcArticle.getFid(),token);
            JSONObject function = funcRes.getData().getJSONObject("function");
            JSONObject funcItem = new JSONObject();
            funcItem.put("id",function.getIntValue("id"));
            funcItem.put("name",function.getString("name"));
            funcArray.add(funcItem);
        }
        JSONObject data = new JSONObject();
        data.put("functions",funcArray);
        return new ResponseDto(200, "成功", data);
    }
}
